const std = @import("std");

const token = @import("./token.zig");

pub const Lexer = struct {
    input: []const u8,
    position: u64,

    pub fn init(input: []const u8) Lexer {
        return .{ .input = input, .position = 0 };
    }

    pub fn next(self: *@This()) !?token.Token {
        var gpa = std.heap.GeneralPurposeAllocator(.{}){};
        const allocator = gpa.allocator();

        var lexeme = std.ArrayList(u8).init(allocator);
        defer lexeme.deinit();

        if (self.position == self.input.len) {
            return null;
        }

        for (self.input[self.position..]) |c| {
            switch (c) {
                ' ' => {
                    self.position += 1;
                    if (lexeme.items.len > 0) {
                        return .{ .token = token.TokenType.WORD, .lexeme = lexeme.toOwnedSlice() };
                    } else {
                        continue;
                    }
                },
                '&' => {
                    if (lexeme.items.len > 0) {
                        return .{ .token = token.TokenType.WORD, .lexeme = lexeme.toOwnedSlice() };
                    } else {
                        self.position += 1;
                        return .{ .token = token.TokenType.AMP, .lexeme = "&" };
                    }
                },
                ';' => {
                    if (lexeme.items.len > 0) {
                        return .{ .token = token.TokenType.WORD, .lexeme = lexeme.toOwnedSlice() };
                    } else {
                        self.position += 1;
                        return .{ .token = token.TokenType.SEMICOLON, .lexeme = ";" };
                    }
                },
                else => try lexeme.append(c),
            }
            self.position += 1;
        }

        return .{ .token = token.TokenType.WORD, .lexeme = lexeme.toOwnedSlice() };
    }
};
