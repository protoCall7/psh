const std = @import("std");

const max_args = 10; // maximum arguments to a command
const max_arg_size = 255; // maximum length of an argument
const max_input = 4096; // maximum length of a command string

pub fn main() !u8 {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    const allocator = gpa.allocator();
    const stdin = std.io.getStdIn().reader();
    const stdout = std.io.getStdOut().writer();

    defer _ = gpa.deinit();

    // ignore SIGINT for now
    const act = std.os.Sigaction{ .handler = .{ .handler = std.os.SIG.IGN }, .mask = std.os.empty_sigset, .flags = 0 };
    try std.os.sigaction(std.os.SIG.INT, &act, null);

    try psh_loop(allocator, stdin, stdout);

    return 0;
}

pub fn psh_loop(allocator: std.mem.Allocator, stdin: std.fs.File.Reader, stdout: std.fs.File.Writer) !void {
    var args: [max_args][max_arg_size:0]u8 = undefined;
    var args_ptrs: [max_args:null]?[*:0]u8 = undefined;

    // outstanding process count
    var op: u64 = 0;

    // setup prompt
    const euid = std.os.linux.geteuid();
    var prompt = "$";

    if (euid == 0) {
        prompt = "#";
    }

    // main loop
    while (true) {
        var bg = false;

        try stdout.print("{s} ", .{prompt});

        const input = read(allocator, stdin);

        if (input) |line| {
            defer allocator.free(line);

            if (line.len == 0) {
                // empty line
                continue;
            }

            var tokens = std.mem.tokenize(u8, line, " ");

            var i: usize = 0;
            while (tokens.next()) |tok| {
                std.mem.copy(u8, &args[i], tok);
                args[i][tok.len] = 0;
                args_ptrs[i] = &args[i];
                i += 1;
            }
            args_ptrs[i] = null;
        } else {
            break;
        }

        // cd builtin
        if (std.cstr.cmp(args_ptrs[0].?, "cd") == 0) {
            std.os.chdirZ(args_ptrs[1].?) catch |err| {
                try stdout.print("ERROR: {}\n", .{err});
            };

            continue;
        }

        // exit builtin
        if (std.cstr.cmp(args_ptrs[0].?, "exit") == 0) {
            break;
        }

        // working on background execution, but it's broken atm
        const exeres = try execute(&args_ptrs, bg);
        if (exeres != null) {
            op += 1;
        }

        if (op > 0) {
            const wpid = std.os.waitpid(-1, 1);
            if (wpid.pid != 0) {
                std.debug.print("{} exited with status {}\n", .{ wpid.pid, wpid.status });
                op -= 1;
            }
        }
    }
}

// reads and returns a line from STDIN, returns null on EOF or error.
pub fn read(allocator: std.mem.Allocator, stdin: std.fs.File.Reader) ?[]u8 {
    const input = stdin.readUntilDelimiterOrEofAlloc(allocator, '\n', max_input) catch |err| {
        // proper error handling goes here
        std.debug.print("ERROR: {}\n", .{err});
        return null;
    };

    return input;
}

pub fn execute(args_ptrs: [*:null]?[*:0]u8, background: bool) !?std.os.pid_t {
    const env = [_:null]?[*:0]u8{null};

    var pid = try std.os.fork();

    if (pid == 0) {
        // child process
        // restore SIGINT behavior
        const chldact = std.os.Sigaction{ .handler = .{ .handler = std.os.SIG.DFL }, .mask = std.os.empty_sigset, .flags = 0 };
        try std.os.sigaction(std.os.SIG.INT, &chldact, null);

        // exec!
        const result = std.os.execvpeZ(args_ptrs[0].?, args_ptrs, &env);
        std.debug.print("ERROR: {}\n", .{result});
    } else {
        // parent process
        if (background) {
            const wpid = std.os.waitpid(pid, 1);
            if (wpid.pid == 0) {
                return pid;
            }
        } else {
            const wpid = std.os.waitpid(pid, 0);
            if (wpid.status != 0) {
                std.debug.print("Process {d} exited with status {d}\n", .{ wpid.pid, wpid.status });
            }
            return null;
        }
    }
    unreachable;
}
