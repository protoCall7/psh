const std = @import("std");

const lexer = @import("./lexer.zig");
const token = @import("./token.zig");

test "parse single word" {
    const input = "exit";
    var lex = lexer.Lexer.init(input);
    const tk = try lex.next();

    if (tk) |t| {
        try std.testing.expect(t.token == token.TokenType.WORD);
        try std.testing.expectEqualStrings(input, t.lexeme);
    }
}

test "parse two words" {
    const input = "cd foo";
    var lex = lexer.Lexer.init(input);
    var tokens: [2]token.Token = undefined;
    var i: u64 = 0;

    while (try lex.next()) |t| {
        tokens[i] = t;
        i += 1;
    }

    try std.testing.expect(tokens[0].token == token.TokenType.WORD);
    try std.testing.expectEqualStrings("cd", tokens[0].lexeme);
    try std.testing.expect(tokens[1].token == token.TokenType.WORD);
    try std.testing.expectEqualStrings("foo", tokens[1].lexeme);
}

test "background" {
    const input = "foo&";
    var lex = lexer.Lexer.init(input);
    var tokens: [2]token.Token = undefined;
    var i: u64 = 0;

    while (try lex.next()) |t| {
        tokens[i] = t;
        i += 1;
    }

    try std.testing.expect(tokens[0].token == token.TokenType.WORD);
    try std.testing.expectEqualStrings("foo", tokens[0].lexeme);
    try std.testing.expect(tokens[1].token == token.TokenType.AMP);
    try std.testing.expectEqualStrings("&", tokens[1].lexeme);
}

test "background 2" {
    const input = "foo &";
    var lex = lexer.Lexer.init(input);
    var tokens: [2]token.Token = undefined;
    var i: u64 = 0;

    while (try lex.next()) |t| {
        tokens[i] = t;
        i += 1;
    }

    try std.testing.expect(tokens[0].token == token.TokenType.WORD);
    try std.testing.expectEqualStrings("foo", tokens[0].lexeme);
    try std.testing.expect(tokens[1].token == token.TokenType.AMP);
    try std.testing.expectEqualStrings("&", tokens[1].lexeme);
}

test "parallel" {
    const input = "foo & bar";
    var lex = lexer.Lexer.init(input);
    var tokens: [3]token.Token = undefined;
    var i: u64 = 0;

    while (try lex.next()) |t| {
        tokens[i] = t;
        i += 1;
    }

    try std.testing.expect(tokens[0].token == token.TokenType.WORD);
    try std.testing.expectEqualStrings("foo", tokens[0].lexeme);
    try std.testing.expect(tokens[1].token == token.TokenType.AMP);
    try std.testing.expectEqualStrings("&", tokens[1].lexeme);
    try std.testing.expect(tokens[2].token == token.TokenType.WORD);
    try std.testing.expectEqualStrings("bar", tokens[2].lexeme);
}

test "parallel 2" {
    const input = "foo& bar";
    var lex = lexer.Lexer.init(input);
    var tokens: [3]token.Token = undefined;
    var i: u64 = 0;

    while (try lex.next()) |t| {
        tokens[i] = t;
        i += 1;
    }

    try std.testing.expect(tokens[0].token == token.TokenType.WORD);
    try std.testing.expectEqualStrings("foo", tokens[0].lexeme);
    try std.testing.expect(tokens[1].token == token.TokenType.AMP);
    try std.testing.expectEqualStrings("&", tokens[1].lexeme);
    try std.testing.expect(tokens[2].token == token.TokenType.WORD);
    try std.testing.expectEqualStrings("bar", tokens[2].lexeme);
}

test "parallel 3" {
    const input = "foo &bar";
    var lex = lexer.Lexer.init(input);
    var tokens: [3]token.Token = undefined;
    var i: u64 = 0;

    while (try lex.next()) |t| {
        tokens[i] = t;
        i += 1;
    }

    try std.testing.expect(tokens[0].token == token.TokenType.WORD);
    try std.testing.expectEqualStrings("foo", tokens[0].lexeme);
    try std.testing.expect(tokens[1].token == token.TokenType.AMP);
    try std.testing.expectEqualStrings("&", tokens[1].lexeme);
    try std.testing.expect(tokens[2].token == token.TokenType.WORD);
    try std.testing.expectEqualStrings("bar", tokens[2].lexeme);
}

test "parallel 4" {
    const input = "foo&bar";
    var lex = lexer.Lexer.init(input);
    var tokens: [3]token.Token = undefined;
    var i: u64 = 0;

    while (try lex.next()) |t| {
        tokens[i] = t;
        i += 1;
    }

    try std.testing.expect(tokens[0].token == token.TokenType.WORD);
    try std.testing.expectEqualStrings("foo", tokens[0].lexeme);
    try std.testing.expect(tokens[1].token == token.TokenType.AMP);
    try std.testing.expectEqualStrings("&", tokens[1].lexeme);
    try std.testing.expect(tokens[2].token == token.TokenType.WORD);
    try std.testing.expectEqualStrings("bar", tokens[2].lexeme);
}

test "sequential" {
    const input = "foo ; bar";
    var lex = lexer.Lexer.init(input);
    var tokens: [3]token.Token = undefined;
    var i: u64 = 0;

    while (try lex.next()) |t| {
        tokens[i] = t;
        i += 1;
    }

    try std.testing.expect(tokens[0].token == token.TokenType.WORD);
    try std.testing.expectEqualStrings("foo", tokens[0].lexeme);
    try std.testing.expect(tokens[1].token == token.TokenType.SEMICOLON);
    try std.testing.expectEqualStrings(";", tokens[1].lexeme);
    try std.testing.expect(tokens[2].token == token.TokenType.WORD);
    try std.testing.expectEqualStrings("bar", tokens[2].lexeme);
}

test "sequential 2" {
    const input = "foo; bar";
    var lex = lexer.Lexer.init(input);
    var tokens: [3]token.Token = undefined;
    var i: u64 = 0;

    while (try lex.next()) |t| {
        tokens[i] = t;
        i += 1;
    }

    try std.testing.expect(tokens[0].token == token.TokenType.WORD);
    try std.testing.expectEqualStrings("foo", tokens[0].lexeme);
    try std.testing.expect(tokens[1].token == token.TokenType.SEMICOLON);
    try std.testing.expectEqualStrings(";", tokens[1].lexeme);
    try std.testing.expect(tokens[2].token == token.TokenType.WORD);
    try std.testing.expectEqualStrings("bar", tokens[2].lexeme);
}

test "sequential 3" {
    const input = "foo ;bar";
    var lex = lexer.Lexer.init(input);
    var tokens: [3]token.Token = undefined;
    var i: u64 = 0;

    while (try lex.next()) |t| {
        tokens[i] = t;
        i += 1;
    }

    try std.testing.expect(tokens[0].token == token.TokenType.WORD);
    try std.testing.expectEqualStrings("foo", tokens[0].lexeme);
    try std.testing.expect(tokens[1].token == token.TokenType.SEMICOLON);
    try std.testing.expectEqualStrings(";", tokens[1].lexeme);
    try std.testing.expect(tokens[2].token == token.TokenType.WORD);
    try std.testing.expectEqualStrings("bar", tokens[2].lexeme);
}

test "sequential 4" {
    const input = "foo;bar";
    var lex = lexer.Lexer.init(input);
    var tokens: [3]token.Token = undefined;
    var i: u64 = 0;

    while (try lex.next()) |t| {
        tokens[i] = t;
        i += 1;
    }

    try std.testing.expect(tokens[0].token == token.TokenType.WORD);
    try std.testing.expectEqualStrings("foo", tokens[0].lexeme);
    try std.testing.expect(tokens[1].token == token.TokenType.SEMICOLON);
    try std.testing.expectEqualStrings(";", tokens[1].lexeme);
    try std.testing.expect(tokens[2].token == token.TokenType.WORD);
    try std.testing.expectEqualStrings("bar", tokens[2].lexeme);
}

test "parallel and sequential" {
    const input = "foo& bar; baz";
    var lex = lexer.Lexer.init(input);
    var tokens: [5]token.Token = undefined;
    var i: u64 = 0;

    while (try lex.next()) |t| {
        tokens[i] = t;
        i += 1;
    }

    try std.testing.expect(tokens[0].token == token.TokenType.WORD);
    try std.testing.expectEqualStrings("foo", tokens[0].lexeme);
    try std.testing.expect(tokens[1].token == token.TokenType.AMP);
    try std.testing.expectEqualStrings("&", tokens[1].lexeme);
    try std.testing.expect(tokens[2].token == token.TokenType.WORD);
    try std.testing.expectEqualStrings("bar", tokens[2].lexeme);
    try std.testing.expect(tokens[3].token == token.TokenType.SEMICOLON);
    try std.testing.expectEqualStrings(";", tokens[3].lexeme);
    try std.testing.expect(tokens[4].token == token.TokenType.WORD);
    try std.testing.expectEqualStrings("baz", tokens[4].lexeme);
}
