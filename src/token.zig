pub const Token = struct {
    token: TokenType,
    lexeme: []const u8,
};

pub const TokenType = enum {
    WORD,
    AMP,
    SEMICOLON,
};
